const SITE_ABOUT = "Join the discord! (PLEASE I need more friends for amongus VR <3)"
const SITE_DISCORD = "https://discord.gg/EdDFcX5byW"
const SITE_NAME = "oscoDOTblog"
const SITE_TAB = "Osco's News!"
const SITE_TITLE = "Monthly-ish updates on my latest schemes!"

export {
  SITE_ABOUT,
  SITE_DISCORD,
  SITE_NAME,
  SITE_TAB,
  SITE_TITLE,
}