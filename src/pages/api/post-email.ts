// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import axios from 'axios'
import type { NextApiRequest, NextApiResponse } from 'next'
import clientPromise from '../../lib/mongodb'

// `await clientPromise` will use the default database passed in the MONGODB_URI
// However you can use another database (e.g. myDatabase) by replacing the `await clientPromise` with the following code:
//
// `const client = await clientPromise`
// `const db = client.db("myDatabase")`
//
// Then you can execute queries against your database like so:
// db.find({}) or any of the MongoDB Node Driver commands

// type Data = {
//   name: string
// }

// Get the bot token from .env.local
const TELEGRAM_BOT_TOKEN = process.env.TELEGRAM_BOT_TOKEN
const TELEGRAM_CHAT_ID = process.env.TELEGRAM_CHAT_ID
const TELEGRAM_API = `https://api.telegram.org/bot${TELEGRAM_BOT_TOKEN}`

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  // Parse Request Body
  const { body } = req
  const { email } = body

  // Get current date and time
  const currentDateTime = new Date()

  // Create human-readable date string in YYYY-MM-DD format
  // Example: "2024-07-24 17:44:12"
  const humanReadableDate = currentDateTime.toLocaleString('en-US', {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
    hour12: false,
    timeZone: 'UTC'
  }).replace(/(\d+)\/(\d+)\/(\d+),/, '$3-$1-$2')

  // Connect to Database
  const client = await clientPromise;
  const db = client.db("Osco");
  let collection = await db.collection("Emails");

  // Insert a document with an upsert option only if it doesn't exist
  let result = await collection.updateOne(
    { 'Email': email}, // Filter
    { 
      $setOnInsert: { 
        'Email' : email,
        'CreatedAt': humanReadableDate
      } 
    }, // Atomic Operator
    { upsert: true } // If Set to True, Adds New Document if it doesn't already exist
  )

  // Send Telegram Webhook Alert
  let telegramAlert = false;

  // Check if BOT_TOKEN is defined
    if (!TELEGRAM_BOT_TOKEN) {
      console.error('TELEGRAM_BOT_TOKEN is not defined in .env.local')
    }
    else {
      // Your bot logic here
      await axios.post(`${TELEGRAM_API}/sendMessage`, {
        chat_id: TELEGRAM_CHAT_ID,
        text: `news.osco.blog just got a new subscriber! ${email}`
      })

      telegramAlert = true;
    }

  const finalResult = {
    email: result.acknowledged,
    webhook: telegramAlert
  }

  res.status(200).json(finalResult)
}