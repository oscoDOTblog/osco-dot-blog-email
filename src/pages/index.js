import { useState } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import { SITE_ABOUT, SITE_DISCORD, SITE_TAB, SITE_TITLE } from '../lib/config';
import Alert from '@mui/material/Alert';
import CircularProgress from '@mui/material/CircularProgress';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Layout, { siteTitle } from '../components/layout';
import utilStyles from '../styles/utils.module.css';

export default function Home() {
  const [email, setEmail] = useState(null); 
  const [loading, setLoading] = useState(false); 
  const [status, setStatus] = useState(null); 

  const mystyle = {
    padding: "5px 50px 25px 50px",
  };

  const joinNewsletter = async (e) => {
    e.preventDefault();
    setLoading(true);
    setStatus("loading")
    if (!email){ 
      alert("Please fill in an email!")
    }
    else {
      console.log("--Adding user to DB...")
      console.log("email: " + email)
      await fetch("../api/post-email", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({"email": email}),
      });
      setStatus("complete")
      console.log("--Adding user complete!")
    }
    setLoading(false)
  };

  return (
    <div>
      <Layout home>
        <Head>
          <title>{SITE_TAB}</title>
          <script defer data-domain="news.osco.blog" src="https://plausible.atemosta.com/js/script.js"></script>
        </Head>        
        <section className={utilStyles.headingMd}>
          <center>
            <h3><i>{SITE_TITLE}</i></h3>
            {(status === "loading") && 
              <div>
                <CircularProgress />
                <br/>
                <br/>
              </div>
            }
            {(status === "complete") && 
              <div>
                <Alert severity="success">Successfully joined newsletter! I'll see you soon :3 </Alert> 
                <br/>
              </div>
            }
            <TextField
              disabled={loading}
              id="filled-disabled"
              label="Email"
              variant="outlined"
              onChange={(e) => setEmail(e.target.value)}
            />
            <br/>
            <br/>
            <Button variant="contained" color="primary" onClick={(e) => joinNewsletter(e)}>Join Newsletter</Button>


            {/* Discord Button */}
            {/* <br/>
            <br/>
            <h4>{SITE_ABOUT}</h4>
            <Link href={SITE_DISCORD} target="_blank" rel="noopener noreferrer">
              <Button 
                variant="contained" 
                style={{ 
                  backgroundColor: '#5865F2', 
                  color: 'white'
                }}
              >
                {"Join Discord"}
              </Button>
            </Link>
            <br/>
            <br/> */}

          </center>
        </section>
      </Layout>
    </div>
  );
}